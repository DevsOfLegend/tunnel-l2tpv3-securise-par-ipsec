#!/bin/bash
# Activation du module gretap
sudo ip link add eoip1 type gretap remote 172.16.184.131 local 172.16.184.129 nopmtudisc
sudo brctl addif bridge_internal eoip1
sudo ip link set dev eoip1 up
