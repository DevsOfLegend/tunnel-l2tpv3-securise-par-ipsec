#!/bin/bash
# on active le mode routeur
sudo sysctl -w net.ipv4.ip_forward=1

# on ajoute deux ponts
sudo brctl addbr bridge_internal
sudo brctl setfd bridge_internal 0
sudo ip link set bridge_internal up

sudo brctl addbr bridge_vlan
sudo brctl setfd bridge_vlan 0
sudo ip link set bridge_vlan up

# on cree les interfaces pour les VLANS des containers
sudo ip link add name hote1_vlan1 type veth peer name veth_hote1
sudo ip link set dev hote1_vlan1 up
sudo ip link set dev veth_hote1 up

sudo ip link add name hote2_vlan2 type veth peer name veth_hote2
sudo ip link set dev hote2_vlan2 up
sudo ip link set dev veth_hote2 up

sudo ip link add name routeur1_vlan1 type veth peer name veth_routeur11
sudo ip link set dev routeur1_vlan1 up
sudo ip link set dev veth_routeur11 up

sudo ip link add name routeur1_vlan2 type veth peer name veth_routeur12
sudo ip link set dev routeur1_vlan2 up
sudo ip link set dev veth_routeur12 up

# on ajoute les interfaces VLANs dans les bridges
sudo brctl addif bridge_vlan veth_hote1
sudo brctl addif bridge_vlan veth_hote2
sudo brctl addif bridge_vlan veth_routeur11
sudo brctl addif bridge_vlan veth_routeur12
