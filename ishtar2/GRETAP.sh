#!/bin/bash
# Activation du module L2TPv3
sudo ip link add eoip1 type gretap remote 172.16.184.129 local 172.16.184.131 nopmtudisc
sudo brctl addif bridge_internal eoip1
sudo ip link set dev eoip1 up
