#!/bin/bash
# Activation du module L2TPv3
sudo modprobe l2tp_eth
sudo modprobe l2tp_ip
sudo ip l2tp add tunnel remote 172.16.184.129 local 172.16.184.131 encap ip tunnel_id 4000 peer_tunnel_id 3000
sudo ip l2tp add session tunnel_id 4000 session_id 2000 peer_session_id 1000
# On change la MTU
sudo ip link set dev l2tpeth0 mtu 1500
# Linkage avec bridge_vlan
sudo brctl addif bridge_vlan l2tpeth0
# Activation du lien
sudo ip link set dev l2tpeth0 up
