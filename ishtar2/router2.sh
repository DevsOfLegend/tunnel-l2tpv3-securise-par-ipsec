#!/bin/bash
route add default gw 10.10.2.254 dev eth0
sudo iptables -t nat -A POSTROUTING -s 172.16.100.0/24 -j MASQUERADE
sudo iptables -t nat -A POSTROUTING -s 172.16.200.0/24 -j MASQUERADE
