#!/bin/bash
# on fixe les adresses des interfaces de l’hote
sudo ip addr add 10.10.2.254/24 dev bridge_internal
sudo iptables -t nat -A POSTROUTING -s 10.10.2.253/24 -j MASQUERADE
